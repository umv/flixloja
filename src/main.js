import Vue from 'vue'

import { store } from './modules/store';
import { router } from './helpers';

import App from './App.vue'

import BootstrapVue from 'bootstrap-vue'
import Loading from 'vue-loading-overlay';
// Import stylesheet

import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'
import '@fortawesome/fontawesome-free/js/all.min'
import 'vue-loading-overlay/dist/vue-loading.css';

Vue.use(Loading, {
  loader: 'spinner',
  width: 150,
  height: 150,
  backgroundColor: '#000000',
  color: '#ffffff'
});
Vue.use(BootstrapVue);

Vue.config.productionTip = false;

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
