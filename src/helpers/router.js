import Vue from 'vue';
import Router from 'vue-router';

import Home from '../views/Home.vue'
import Steps from '../views/Steps.vue'
import Results from '../views/Results.vue'
import Login from '../views/Login.vue'
import FlixAuth from '../components/flixdovarejo/FlixAuth.vue'

Vue.use(Router);

export const router = new Router({
    mode: 'history',
    base: process.env.BASE_URL,
    routes: [
        {
            path: '/',
            name: 'home',
            component: Home
        },
        {
            path: '/steps',
            name: 'steps',
            component: Steps
        },
        {
            path: '/results',
            name: 'results',
            component: Results
        },
        {
            path: '/login',
            name: 'Login',
            component: Login
        },
        {
            path: '/auth/flix',
            name: 'FlixAuth',
            component: FlixAuth
        },

        // otherwise redirect to home
        {
            path: '*',
            redirect: '/'
        }
    ]
});

router.beforeEach((to, from, next) => {
    // redirect to login page if not logged in and trying to access a restricted page
    const publicPages = ['/login', '/auth/flix'];
    const authRequired = !publicPages.includes(to.path);
    const loggedIn = localStorage.getItem('user');

    if(authRequired){

        if(!loggedIn || loggedIn.expires_in < (Date.now() / 1000 | 0)) {

            return next('/login');

            /*if (loggedIn.origin_in === 'flix') {

                this.$store.dispatch('authentication/logout');
                document.location.href = `${config.url.flixdovarejo}`;

            } else {

                return next('/login');

            }*/

        }

    }

    next();

})
